# API for Delivery Service

```
Language: PHP
Framework: Laravel
```

The route must not perform a direct delivery between the origin and destination, for example, if a delivery must be made from point A to point B, it must choose the routes that pass through intermediate points, as for example, A - C - B.

Each route has a cost and time associated so it is possible to retrieve the best option depending on the cost or time (in the figure below, there are multiple options to deliver from A to B, however the route A-C-B has the lower time to deliver). 

---
## Credentials
ROLE | EMAIL | PASSWORD
--- | --- | ----
**`ADMIN`** | admin@gmail.com | admin
**`USER`** | user@gmail.com | user

Only admin has access to create, update, delete new points or route.


## API Endpoint

```
Header:
    Authorization: Bearer {token}
```

METHOD | URI | FIELD | REQUIRED FIELD | DESCRIPTION
--- | --- | ---- | --- | ---
**`POST`** | api/login | email, password | all | login for token
**`GET`** | api/route | origin, destination | all | get for shortest path
**`POST`** | api/route | origin, destination, cost, time | all| add route
**`PUT`** | api/route/{id} | id, origin, destination, cost, time | all| update route
**`DELETE`** | api/route/{id} | id | all| delete route
